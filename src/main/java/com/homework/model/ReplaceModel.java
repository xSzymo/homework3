package com.homework.model;

import java.io.File;

public class ReplaceModel {
    private String oldValue;
    private String newValue;
    private Header header;
    private File originalFile;
    private File newFile;

    public ReplaceModel(String oldValue, String newValue, Header header, File originalFile, File newFile) {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.header = header;
        this.originalFile = originalFile;
        this.newFile = newFile;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public File getOriginalFile() {
        return originalFile;
    }

    public void setOriginalFile(File originalFile) {
        this.originalFile = originalFile;
    }

    public File getNewFile() {
        return newFile;
    }

    public void setNewFile(File newFile) {
        this.newFile = newFile;
    }
}
