package com.homework.model;

public enum Header {
    FILENAME(0), ORIGIN(1), METADATA(2), HASH(3);

    private int index;

    Header(int i) {
        index = i;
    }

    public int getIndex() {
        return index;
    }
}
