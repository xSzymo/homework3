package com.homework.services;

public interface ReplacerI<E> {
    void execute(E e) throws Exception;
}
