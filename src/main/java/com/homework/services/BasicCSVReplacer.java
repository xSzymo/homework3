package com.homework.services;

import com.homework.model.ReplaceModel;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.FileReader;
import java.io.FileWriter;

public class BasicCSVReplacer implements ReplacerI<ReplaceModel> {
    @Override
    public void execute(ReplaceModel model) throws Exception {
        CSVReader reader = new CSVReader(new FileReader(model.getOriginalFile()));
        CSVWriter writer = new CSVWriter(new FileWriter(model.getNewFile()));
        int index = model.getHeader().getIndex();
        String[] currentLine;

        while ((currentLine = reader.readNext()) != null) {
            if (currentLine.length > index && currentLine[index].equals(model.getOldValue()))
                currentLine[index] = model.getNewValue();
            writer.writeNext(currentLine);
        }

        writer.close();
        reader.close();
    }
}
