package com.homework;

import com.homework.model.Header;
import com.homework.model.ReplaceModel;
import com.homework.services.BasicCSVReplacer;
import com.homework.services.ReplacerI;

import java.io.File;

public class Main {
    private static ReplacerI<ReplaceModel> replacer;

    public static void main(String... args) throws Exception {
        ReplaceModel model = new ReplaceModel(" Londom", "London", Header.ORIGIN, new File("old.csv"), new File("new.csv"));
        replacer = new BasicCSVReplacer();
        replacer.execute(model);
    }
}
